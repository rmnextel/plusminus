package main

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/flosch/pongo2"
	"github.com/gorilla/mux"
	"github.com/jmcvetta/randutil"
)

var Template = pongo2.Must(pongo2.FromFile("templates/index.html"))
var Start = pongo2.Must(pongo2.FromFile("templates/start.html"))
var End = pongo2.Must(pongo2.FromFile("templates/end.html"))
var primersCounter int

var DelimetrArray = []string{"+", "-"}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/main", MainControl)
	router.HandleFunc("/reset", Reset)
	router.HandleFunc("/", StartControl)
	http.ListenAndServe(":8080", router)

}

func StartControl(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		err := r.ParseForm()
		if err != nil {
			return
		}

		answer := r.PostFormValue("qest")
		fmt.Println(answer)
		primersCounter, _ = strconv.Atoi(answer)
		http.Redirect(w, r, "/main", 302)
	}
	Start.ExecuteWriter(nil, w)

}

func Reset(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		http.Redirect(w, r, "/", 302)
	}

}

func MainControl(w http.ResponseWriter, r *http.Request) {
	choiceDelimetr, _ := randutil.ChoiceString(DelimetrArray)
	one, two, etalon := GeneratePrimers(choiceDelimetr)

	if r.Method == "POST" {
		r.ParseForm()
		answer := r.FormValue("answer")
		answerEtlone := r.FormValue("etalon")
		if answerEtlone != answer {
			primersCounter = primersCounter + 3
			errs := errors.New("не правильно, попробуй еше раз")
			Template.ExecuteWriter(pongo2.Context{"error": errs, "primersCounter": primersCounter, "one": one, "delimetr": choiceDelimetr, "two": two, "etalon": etalon}, w)
			return
		} else {
			primersCounter = primersCounter - 1
			if primersCounter != 0 {
				Template.ExecuteWriter(pongo2.Context{"primersCounter": primersCounter, "one": one, "delimetr": choiceDelimetr, "two": two, "etalon": etalon}, w)
				return
			} else {
				End.ExecuteWriter(nil, w)
				return
			}

		}

	}

	Template.ExecuteWriter(pongo2.Context{"primersCounter": primersCounter, "one": one, "delimetr": choiceDelimetr, "two": two, "etalon": etalon}, w)
	return
}

func GeneratePrimers(delimetr string) (int, int, int) {
	toNumber := 50
	if delimetr == "+" {

		plusone, _ := randutil.IntRange(0, toNumber)
		plustwo, _ := randutil.IntRange(0, toNumber)
		plusanswer := plusone + plustwo
		return plusone, plustwo, plusanswer
	}
	one, _ := randutil.IntRange(0, toNumber)
	two, _ := randutil.IntRange(0, one)
	answer := one - two
	return one, two, answer
}
